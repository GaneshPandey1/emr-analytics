from flask import Flask, render_template, request
import pandas as pd
import MySQLdb
from mysql.connector import Error
import mysql.connector

try:
    connection = mysql.connector.connect(host='34.67.178.91',

                                         database='emr_dev',
                                         user='emr_dev',
                                         password='emr_dev@123')

    sql_select_doctors = "select * from doctors"
    cursor = connection.cursor()
    cursor.execute(sql_select_doctors)
    records = cursor.fetchall()

    sql_doctor_column_names = "show columns FROM emr_dev.doctors"
    cursor.execute(sql_doctor_column_names)
    records1 = cursor.fetchall()
    column_names = []
    for rows in records1:
        column_names.append(rows[0])

    # print(column_names)
    doctors_dataframe = pd.DataFrame(records, columns = column_names)

    # print(doctors_dataframe)

    sql_select_appointments = "select * from appointments"
    cursor = connection.cursor()
    cursor.execute(sql_select_appointments)
    all_appointment = cursor.fetchall()

    sql_appointment_column_names = "show columns FROM emr_dev.appointments"
    cursor.execute(sql_appointment_column_names)
    appointments_columns = cursor.fetchall()
    column_names_appointment = []
    for rows in appointments_columns:
        column_names_appointment.append(rows[0])

    # print(column_names)
    appointment_dataframe = pd.DataFrame(all_appointment, columns=column_names_appointment)

    print(appointment_dataframe)



except Error as e:
    print("Error reading data from MySQL table", e)
finally:
    if (connection.is_connected()):
        connection.close()
        cursor.close()
        print("MySQL connection is closed")